#include <iostream>
#include "bib.h"

int busca_binaria_recursiva (int *v[], int n , int x) {
	if (n <= 0) {
		return 0 ;
	}

	int indice = n/2 ;
	else if (x == v[indice]) {
		return 1 ;
	}

	else if (x > v[indice]) {
		return busca_binaria_recursiva (v[indice+1], n , x) ;
	}

	else {
		return busca_binaria_recursiva (v , indice-1, x) ;
	}
}

int busca_binaria_interativa (int *v[], int n , int x) {
	int indice , ini = 0 , fim = n-1 ;

	while (ini <= fim) {
		indice = (ini+fim)/2 ;
		if (x == v[indice]) {
			return 1 ;
		}
		else if (x > v[indice]) {
			ini = indice+1 ;
		}
		else {
			fim = indice-1 ;
		}
	}
	return 0 ;
}

int busca_sequencial_recursiva (int *v[], int n, int x) {

	if (n < 0) {
		return 0 ;
	}

	else if (x == v[n-1]) {
		return 1 ;
	}
	else {
		return busca_sequencial_recursiva (v, n-1 , x) ;
	}
}

int busca_sequencial_interativa (int *v[], int n , int x) {
	int indice = 0 ;

	while (indice <= n) {
		if (x == v[indice]) {
			return 1 ;
		}
		else {
			indice++ ;
		}
	}

	return 0 ;
}