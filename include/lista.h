#ifndef LISTA_H
#define LISTA_H

#include <iostream>


using std::cout ;
using std::endl ;



/**
 * @class 	ListaLigada lista.h
 * @brief	Classe que respresenta uma lista generica
 * @details Os atributos da lista são três structs que contem uma classe generica e um apontador que aponta
 *			para o proximo node da lista, sendo definidos metodos para adicionar elementos, remover elementos,
 *			mostrar os elementos da lista e verificar o seu tamanho. 
*/


struct Node {
		int data ;
		Node *prox ;
	} ;



class ListaLigada {
	


	public:
		ListaLigada () ;
		void add (int d) ;
		void Delete (int d) ;
		void printList () ;		
		int getTamanho() ;
		Node* getHead () ;			

	private:
		Node *head ;
		Node *curr ;
		Node *temp ;
		int tamanho ;

} ;




#endif /* LISTA_H  */