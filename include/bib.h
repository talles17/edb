#ifdef BIB_H
#define BIB_H

int busca_binaria_recursiva (Node *ptr, int n , int x) ;

int busca_binaria_interativa (Node *ptr, int n , int x) ;

int busca_sequencial_recursiva (Node *ptr, int n, int x) ;

int busca_sequencial_interativa (Node *ptr, int n , int x) ;

void insertion_sort (Node *ptr, int n) ;


#endif /* BIB_H */