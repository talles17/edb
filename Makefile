RM= rm -rf
CC=g++

LIB_DIR=./lib
INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
TEST_DIR=./test

CFLAGS = -Wall -I. -I$(INC_DIR)

.PHONY: all debug

all: executavel 

debug: -g -O0
debug: executavel

executavel: $(OBJ_DIR)/func.o $(OBJ_DIR)/lista.o $(OBJ_DIR)/ordenacao.o $(OBJ_DIR)/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'executavel' criado em $(BIN_DIR)] +++"
	@echo "============="

$(OBJ_DIR)/func.o: $(SRC_DIR)/func.cpp $(INC_DIR)/bib.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/lista.o: $(SRC_DIR)/lista.cpp 
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/ordenacao.o: $(SRC_DIR)/ordenacao.cpp
	$(CC) -c $(CFLAGS) -o $@ $< 

$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp 
	$(CC) -c $(CFLAGS) -o $@ $< 



clean:
	$(RM) $(DOC_DIR)/*
	$(RM) $(OBJ_DIR)/* 	