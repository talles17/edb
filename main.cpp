#include <iostream>

#include "bib.h"

using std::cout ;
using std::endl ;


int main () {
	int vetor[30] = { 4 , 43 , 20 , 85 , 2 , 9 , 67 , 72 , 55 , 10 ,
					 27 , 99 , 33 , 65 , 49 , 76 , 52 , 18 , 89 ,
					 54 , 23 , 73 , 55 , 64 , 32 , 12 , 6 , 11 ,16 } ;
	int chave = 9 ;
	
	int s ;
	
	s = busca_binaria_recursiva(&vetor, 30 , chave ) ;

	if (s == 1) {
		cout << "Achou" << endl ; 
	}
	else {
		cout << "Nao achou" << endl ;
	}

	return 0 ;
}