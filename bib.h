#ifdef BIB_H
#define BIB_H

int  busca_binaria_recursiva (int *v[], int n , int x) ;

int  busca_binaria_interativa (int *v[], int n , int x) ;

int busca_sequencial_recursiva (int *v[], int n, int x) ;

int  busca_sequencial_interativa (int *v[], int n , int x) ;


#endif /* BIB_H */