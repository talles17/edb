#include <iostream>

#include "bib.h"
#include "lista.h"

int busca_binaria_recursiva (Node *ptr, int n , int x) {
	Node *head = new Node ;
	head = ptr ;
	int indice = n/2 ;

	if (n <= 0) {
		return 0 ;
	}

	else {	

		for (int ii = 1 ; indice!= ii ; ii++ ) {
			ptr = ptr->prox ;
		} 	
	
		if (x == ptr->data) {
			return 1 ;
		}

		else if (x > ptr->data) {
			return busca_binaria_recursiva (ptr, n/2  , x) ;
		}

		else {
			return busca_binaria_recursiva (head , n/2, x) ;
		}
	}
}

int busca_binaria_interativa (Node *ptr, int n , int x) {
	int indice  ;
	Node *head = new Node ;
	while (0 < n) {
		indice = n/2 ;
		head = ptr ;
		while (ptr->prox != NULL) {
			ptr = ptr->prox ;
		}

		if (x == ptr->data) {
			return 1 ;
		}
		else if (x > ptr->data) {
			n = n - indice ;
		}
		else {
			ptr = head ;
			n = n - indice ;
		}
	}
	return 0 ;
} 

int busca_sequencial_recursiva (Node *ptr, int n, int x) {

	if (n < 0) {
		return 0 ;
	}
	else {
		while (ptr->prox != NULL) {
			ptr = ptr->prox ;
		}

		if (x == ptr->data) {
			return 1 ;
		}
		else {
			return busca_sequencial_recursiva (ptr, n-1 , x) ;
		}
	}	
}

int busca_sequencial_interativa (Node *ptr, int n , int x) {
	int indice = 1 ;

	while (indice <= n) {
		if (x == ptr->data) {
			return 1 ;
		}
		else {
			indice++ ;
			ptr = ptr->prox ;
		}
	}

	return 0 ;
}

