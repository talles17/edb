
#include <iostream>
using std::cout ;
using std:: endl ;

#include "lista.h"

/**
 * @details	Construtor que inicia a lista apontando os ponteiros de structs para o vazio.
*/

ListaLigada::ListaLigada () {
	head = NULL ;
	curr = NULL ;
	temp = NULL ;
	tamanho = 0 ;
}

/**
 * @param d Dado a ser adicionado na lista
*/

void ListaLigada::add (int d) {
	Node *n = new Node() ;
	n->prox = NULL ;
	n->data = d ;

	if (head != NULL ) {
		curr = head ;
		while (curr->prox != NULL ) {
			curr = curr->prox ;
		}
		curr->prox = n ;
		tamanho++ ;
	}
	else {
		head = n ;
		tamanho++ ;
	}
}
/**
 * @param d Dado a ser removido da lista
*/

void ListaLigada::Delete (int d) {
	Node *n = NULL ;
	temp = head ;
	curr = head ;
	while (curr != NULL && curr->data != d) {
		temp = curr ;
		curr = curr->prox ;
	}
	if (curr == NULL) {
		cout << d << " Nao esta na lista." << endl ;
		delete n ;
	}
	else {
		n = curr ;
		curr = curr->prox ;
		temp->prox = curr ;
		delete n ;
		cout << "O " << d << " foi deletado da lista." << endl ;
		tamanho-- ;
	}
}
/**
 * @details Metodo que imprime a lista
*/

void ListaLigada::printList () {
	curr = head ;
	while (curr != NULL) {
		cout << curr->data << endl ;
		curr = curr->prox ;
	}
}


int ListaLigada::getTamanho () {
	return tamanho ;
}


Node* ListaLigada::getHead () {
	return head ;
}




			

